package piece;

import chessgame.Board;

public class Bishop extends Piece {
	public Bishop(int color, String name) {
		super(color, name);
	}
	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		if(x1 == x2 || y1 == y2) return false;
		if(Math.abs(x1 - x2) != Math.abs(y1 - y2)) return false;
		
		if(x1 > x2) {
			if(y1 < y2) {
				y1 = y1 + 1;
				for(int i = x1 - 1; i > x2; i--) {
					if(Board.isOccupied(i, y1)) {
						return false;
					}
					y1++;
				}
			}else {
				y1 = y1 - 1; 
				for(int i = x1 - 1; i > x2; i--) {
					if(Board.isOccupied(i, y1)) {
						return false;
					}
					y1--;
				}
			}
		}else {
			if(y1 < y2) {
				y1 = y1 + 1;
				for(int i = x1 + 1; i < x2; i++) {
					if(Board.isOccupied(i, y1)) {
						return false;
					}
					y1++;
				}
				
			}else {
				y1 = y1 - 1;
				for(int i = x1 + 1; i < x2; i++) {
					if(Board.isOccupied(i, y1)) {
						return false;
					}
					y1--;
				}
				
			}
		}
		if(Board.board[x2][y2].color == color) return false;
		
		return true;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {
		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1,colortile);
		if(color == 0) {
			Board.board[x2][y2] = new Bishop(0," wB");

			
		}else {
			Board.board[x2][y2] =new Bishop(1," bB");
		}

	
	}
}
