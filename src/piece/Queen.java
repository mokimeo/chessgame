package piece;

import chessgame.Board;

public class Queen extends Piece {
	public Queen(int color,String name) {
		super(color, name);
	}
	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		
		Piece rook = new Rook(color,"");
		Piece bishop= new Bishop(color,"");
		if(rook.isValidMove(color, x1, y1, x2, y2) || bishop.isValidMove(color, x1, y1, x2, y2)) {
			return true;
		}
		return false;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {
		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1,colortile);
		if(color == 0) {
			Board.board[x2][y2] = new Queen(0," wQ");

			
		}else {
			Board.board[x2][y2] = new Queen(1," bQ");
		}
		
	}
}
