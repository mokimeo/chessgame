package piece;

import chessgame.Board;

public class Pawn extends Piece {

	public Pawn(int color, String name) {
		super(color, name);

	}

	public boolean initialpiece(int color, int x1) {
		if (color == 0 && x1 == 6)
			return true;
		if (color == 1 && x1 == 1)
			return true;
		return false;
	}

	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		System.out.println("pawn");
		if (color == 0) {
			if (x1 <= x2)
				return false;
		} else {
			if (x1 >= x2)
				return false;
		}
		if (y1 == y2) {

			if (Math.abs(x1 - x2) > 2)
				return false;
			if (Math.abs(x1 - x2) == 2) {
				if (!initialpiece(color, x1))
					return false;
			}
			if (color == 0) {

				if (Board.isOccupied(x1 - 1, y1))
					return false;

				if (Board.isOccupied(x1 - 2, y1))
					return false;

			} else {
				if (Board.isOccupied(x1 + 1, y1))
					return false;

				if (Board.isOccupied(x1 + 2, y1))
					return false;
			}

		} else {
			if (Math.abs(y2 - y1) != 1 || Math.abs(x2 - x1) != 1) {
				return false;
			}
			if (!Board.isOccupied(x2, y2)) {
				return false;
			}
		}

		return true;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {

		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1, colortile);
		if (color == 0) {
			Board.board[x2][y2] = new Pawn(0, " wP");

		} else {
			Board.board[x2][y2] = new Pawn(1, " bP");
		}
		if (Board.CheckEnPassant(Board.board[x1][y1].color, y1, x2, y2)) {
			if (Board.isOccupied(x2 - 1, y2)) {
				colortile = Board.getcolor(x2 - 1, y2);
				Board.board[x2 - 1][y2] = new EmptyPiece(-1, colortile);
			} else if (Board.isOccupied(x2 + 1, y2)) {
				colortile = Board.getcolor(x2 + 1, y2);
				Board.board[x2 + 1][y2] = new EmptyPiece(-1, colortile);
			}

		}

	}
}
