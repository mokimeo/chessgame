package piece;

import chessgame.Board;

public class Knight extends Piece {
	public Knight(int color,String name) {
		super(color, name);
	}

	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		
		if(Math.abs(x1 - x2) == 2 && Math.abs(y1 - y2) == 1) {
			return true;
		}
		if(Math.abs(x1 - x2) == 1 && Math.abs(x2 - x2) == 2) {
			return true;
		}
		
		return false;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {
		
		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1,colortile);
		if(color == 0) {
			Board.board[x2][y2] = new Knight(0," wK");

			
		}else {
			Board.board[x2][y2] =new Knight(1," bK");
		}
	}
}
