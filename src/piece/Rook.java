package piece;

import chessgame.Board;

public class Rook extends Piece {
	public Rook(int color,String name) {
		super(color, name);
	}

	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		if(!(x1 == x2 || y1 == y2)) return false;
		if(x1 == x2) {
			if(y1 == y2) return false;
			if(y1 < y2) {
				for(int i = y1 + 1; i < y2; i++) {
					if(Board.isOccupied(x1, i) ) {
						return false;
					}
				}
				
				
			}else {
				for(int i = y1 - 1; i > y2; i--) {
					if(Board.isOccupied(x1, i)) {
						return false;
					}
				}
			}
		
		}
		if(y1 == y2) {
			if(x1 == x2) return false;
			if(x1 > x2) {
				for(int i = x1 - 1; i > x2; i--) {
					if(Board.isOccupied(i, y1)) {
						return false;
					}
				}
			}else {
				for(int i = x1 + 1; i < x2; i++) {
					if(Board.isOccupied(i, y1)) {
						return false;
					}
				}
			}
		}
		if(Board.board[x2][y2].color == color) return false;
		
		return true;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {
		
		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1,colortile);
		if(color == 0) {
			Board.board[x2][y2] = new Rook(0," wR");

			
		}else {
			Board.board[x2][y2] =new Rook(1," bR");
		}

		
	}
}
