package piece;
import chessgame.*;
public abstract class Piece {
	
	public String name;
	public int color;

	public Piece(int color,String name) {
		
		this.name = name;
		this.color = color;
	}
	public abstract boolean isValidMove(int color, int x1, int y1, int x2, int y2);
	public abstract void Move(int color, int x1, int y1, int x2, int y2);
	

	
}
