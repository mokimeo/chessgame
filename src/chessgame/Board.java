package chessgame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import java.util.StringTokenizer;

import piece.*;

public class Board {
	public static final Piece[][] board = createBoard();
	private static final int resign = 100;
	private static final int draw = 101;
	public static int turn = 0;

	static int[] oldcoordinate = new int[2];
	static int[] newcoordinate = new int[2];

	private static Piece[][] createBoard() {
		final Piece[][] tempboard = new Piece[9][9];
		String color = "   ";
		// border
		tempboard[0][8] = new EmptyPiece(-1, "  8");
		tempboard[1][8] = new EmptyPiece(-1, "  7");
		tempboard[2][8] = new EmptyPiece(-1, "  6");
		tempboard[3][8] = new EmptyPiece(-1, "  5");
		tempboard[4][8] = new EmptyPiece(-1, "  4");
		tempboard[5][8] = new EmptyPiece(-1, "  3");
		tempboard[6][8] = new EmptyPiece(-1, "  2");
		tempboard[7][8] = new EmptyPiece(-1, "  1");

		tempboard[8][0] = new EmptyPiece(-1, "  a");
		tempboard[8][1] = new EmptyPiece(-1, "  b");
		tempboard[8][2] = new EmptyPiece(-1, "  c");
		tempboard[8][3] = new EmptyPiece(-1, "  d");
		tempboard[8][4] = new EmptyPiece(-1, "  e");
		tempboard[8][5] = new EmptyPiece(-1, "  f");
		tempboard[8][6] = new EmptyPiece(-1, "  g");
		tempboard[8][7] = new EmptyPiece(-1, "  h");
		tempboard[8][8] = new EmptyPiece(-1, "  ");
		// team black initialize
		tempboard[0][0] = new Rook(1, " bR");
		tempboard[0][1] = new Knight(1, " bN");
		tempboard[0][2] = new Bishop(1, " bB");
		tempboard[0][3] = new Queen(1, " bQ");
		tempboard[0][4] = new King(1, " bK");
		tempboard[0][5] = new Bishop(1, " bB");
		tempboard[0][6] = new Knight(1, " bN");
		tempboard[0][7] = new Rook(1, " bR");

		tempboard[1][0] = new Pawn(1, " bp");
		tempboard[1][1] = new Pawn(1, " bp");
		tempboard[1][2] = new Pawn(1, " bp");
		tempboard[1][3] = new Pawn(1, " bp");
		tempboard[1][4] = new Pawn(1, " bp");
		tempboard[1][5] = new Pawn(1, " bp");
		tempboard[1][6] = new Pawn(1, " bp");
		tempboard[1][7] = new Pawn(1, " bp");

		// team white initialize

		tempboard[7][0] = new Rook(0, " wR");
		tempboard[7][1] = new Knight(0, " wN");
		tempboard[7][2] = new Bishop(0, " wB");
		tempboard[7][3] = new Queen(0, " wQ");
		tempboard[7][4] = new King(0, " wK");
		tempboard[7][5] = new Bishop(0, " wB");
		tempboard[7][6] = new Knight(0, " wN");
		tempboard[7][7] = new Rook(0, " wR");

		tempboard[6][0] = new Pawn(0, " wp");
		tempboard[6][1] = new Pawn(0, " wp");
		tempboard[6][2] = new Pawn(0, " wp");
		tempboard[6][3] = new Pawn(0, " wp");
		tempboard[6][4] = new Pawn(0, " wp");
		tempboard[6][5] = new Pawn(0, " wp");
		tempboard[6][6] = new Pawn(0, " wp");
		tempboard[6][7] = new Pawn(0, " wp");

		for (int i = 2; i <= 5; i++) {

			for (int j = 0; j < 8; j++) {

				color = getcolor(i, j);
				tempboard[i][j] = new EmptyPiece(-1, color);
				// System.out.print(tempboard[i][j].name);

			}
			// System.out.println();
		}

		return tempboard;
	}

	public static boolean isOccupied(int x, int y) {
		if (board[x][y].name.equals(" ##") || board[x][y].name.equals("   ")) {
			return false;
		}
		return true;
	}

	public static String getcolor(int i, int j) {
		if ((i % 2 == 0 && j % 2 == 1) || (i % 2 == 1 && j % 2 == 0)) {
			return " ##";
		}
		return "   ";
	}

	public static void initializeboard() {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				System.out.print(board[i][j].name);
			}
			System.out.println();
		}

	}

	public static int AlphaToInt(char c) {
		if (c == 'a')
			return 0;
		if (c == 'b')
			return 1;
		if (c == 'c')
			return 2;
		if (c == 'd')
			return 3;
		if (c == 'e')
			return 4;
		if (c == 'f')
			return 5;
		if (c == 'g')
			return 6;
		if (c == 'h')
			return 7;
		return -1;
	}

	public static void releaseTile() {

	}

	public static void occupySpot() {

	}

	public static boolean CheckEnPassant(int color, int y1, int x2, int y2) {

		if (y1 != y2) {
			if ((x2 - 1) > 0 && isOccupied(x2 - 1, y2)) {
				System.out.println("check enpassant - 1" + board[x2 - 1][y2].name + " " + board[x2 - 1][y2].color);

				if (board[x2 - 1][y2] instanceof Pawn) {

					if (color != board[x2 - 1][y2].color) {

						return true;
					}

				}
			}

			if (x2 + 1 < 8 && isOccupied(x2 + 1, y2)) {
				System.out.println("check enpassant + 1" + board[x2 + 1][y2].name + " " + board[x2 + 1][y2].color);
				if (board[x2 + 1][y2] instanceof Pawn) {
					if (color != board[x2 + 1][y2].color) {
						return true;
					}

				}
			}
		}

		return false;
	}

	public static void ExecuteMove() {
		int x1 = oldcoordinate[0];
		int y1 = oldcoordinate[1];
		int x2 = newcoordinate[0];
		int y2 = newcoordinate[1];

		if (turn == 0) {

			if (isOccupied(x1, y1) && isValidPiece(x1, y1)) {
				System.out.println("white move");
				System.out.println("piece is :" + board[x1][y1].name + ",and color :" + board[x1][y1].color);
				if (board[x1][y1].isValidMove(board[x1][y1].color, x1, y1, x2, y2)) {
					System.out.println("white move");
					System.out.println("valid move");
					board[x1][y1].Move(board[x1][y1].color, x1, y1, x2, y2);
					turn = 1;
					initializeboard();
					return;

				} else {
					if (board[x1][y1] instanceof Pawn) {

						if (CheckEnPassant(board[x1][y1].color, y1, x2, y2)) {
							System.out.println("valid en passant");
							board[x1][y1].Move(board[x1][y1].color, x1, y1, x2, y2);
							initializeboard();
							turn = 1;
							return;
						}
					}
					System.out.println("Invalid move, Try again!");
					return;
				}

			} else {
				System.out.println("Invalid move, Try again!");
				return;
			}
		} else {
			if (isOccupied(x1, y1) && isValidPiece(x1, y1)) {
				System.out.println("black move");
				System.out.println("piece is :" + board[x1][y1].name + ",and color :" + board[x1][y1].color);
				if (board[x1][y1].isValidMove(board[x1][y1].color, x1, y1, x2, y2)) {
					System.out.println("valid move");
					board[x1][y1].Move(board[x1][y1].color, x1, y1, x2, y2);
					turn = 0;
					initializeboard();
					return;

				} else {
					if (board[x1][y1] instanceof Pawn) {
						if (CheckEnPassant(board[x1][y1].color, y1, x2, y2)) {
							System.out.println("valid en passant");
							board[x1][y1].Move(board[x1][y1].color, x1, y1, x2, y2);
							initializeboard();
							turn = 0;
							return;
						}
					}
					System.out.println("Invalid move, Try again!");

					return;
				}

			} else {
				System.out.println("Invalid move, Try again!");

				return;

			}
		}

	}

	public static int[] getXY(String s) {
		int[] result = new int[2];
		if (Character.isLetter(s.charAt(0)) && Character.isDigit(s.charAt(1))) {
			int col = AlphaToInt(s.charAt(0));
			if (col == -1) {
				System.out.println("Not in Domain");
				return null;
			}
			int row = Character.getNumericValue(s.charAt(1));
			if (row < 1 || row > 8) {
				System.out.println("Not in Range");
				return null;
			}
			result[0] = 8 - row;
			result[1] = col;

		}
		return result;

	}

	public static void GetCoordinate(ArrayList<String> ar) {
		if (ar.get(0).length() > 2 || ar.get(1).length() > 2) {
			System.out.println("Wrong format");
			return;
		}
		oldcoordinate = getXY(ar.get(0));
		if (oldcoordinate == null)
			return;
		newcoordinate = getXY(ar.get(1));
		if (newcoordinate == null)
			return;
		System.out.println("row: " + oldcoordinate[0] + ", col: " + oldcoordinate[1]);
		System.out.println("newrow: " + newcoordinate[0] + ", newcol: " + newcoordinate[1]);
		ExecuteMove();
	}

	public static void BreakDownInput(String input) {

		if (input == null) {
			System.out.println("illegal input, try again!");
			return;
		}
		StringTokenizer st = new StringTokenizer(input, " ");
		ArrayList<String> ar = new ArrayList<String>();
		while (st.hasMoreTokens()) {
			ar.add(st.nextToken());
		}
		if (ar.size() > 3) {
			System.out.println("Illegal input, try again");
			return;
		}
		if (ar.size() == 2) {
			GetCoordinate(ar);
		}

	}

	public static boolean isValidPiece(int x, int y) {
		if (isOccupied(x, y)) {
			if (turn != Board.board[x][y].color) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) throws IOException {

		boolean start = true;
		initializeboard();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = null;
		while (start) {

			input = null;

			if (turn == 0) {
				System.out.print("White's move:");

			} else if (turn == 1) {
				System.out.print("Black's move:");

			}

			input = br.readLine();
			BreakDownInput(input.toLowerCase());

		}

	}

}
